//
//  EmailChoicePresenter.swift
//  Open Mail
//
//  Created by Berend, Dexter on 8/21/19.
//  Copyright © 2019 Berend, Dexter. All rights reserved.
//

import UIKit

protocol EmailChoicePresenter {
    
    var emailChoiceSheet: UIAlertController? { get }
    var emailApps: [String : (String, String)] { get }
    
    func setupEmailChoiceSheet(withTitle title: String?, withFrom emailString: String?) -> UIAlertController
}

// set up the choice sheet
// define the required setup function
extension EmailChoicePresenter {
    
    func setupEmailChoiceSheet(withTitle title: String? = "Choose Email", withFrom emailString: String? = nil) -> UIAlertController {
        
        let emailActionSheet = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
        
        // let actionArray = populateAppActionArray(withEmailFrom: emailString)
        let actionArray = populateActionArray(sendTo: "timcook@apple.com")
        
        if !actionArray.isEmpty {
            actionArray.forEach( { action in
                emailActionSheet.addAction(action)
            })
        }
        
        emailActionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        return emailActionSheet
    }
}

// function to list the email apps
extension EmailChoicePresenter {
    fileprivate func populateAppActionArray(withEmailFrom emailString: String?) -> [UIAlertAction] {
        
        var actionArray = [UIAlertAction]()
        
        if let gmail = openGmailAction(withFrom: emailString) {
            actionArray.append(gmail)
        }
        if let outlook = openOutlookAction(withFrom: emailString) {
            actionArray.append(outlook)
        }
        if let spark = openSparkAction(withFrom: emailString) {
            actionArray.append(spark)
        } 
        if let mail = openMailAction(withFrom: emailString) {
            actionArray.append(mail)
        }
        
        
        return actionArray
    }
    
    fileprivate func populateActionArray(sendTo: String?) -> [UIAlertAction] {
        var actionArray = [UIAlertAction]()
        
        emailApps.forEach { (app, scheme) in
            let openApp = makeEmailAction(pair: (app, scheme))
            if let mailApp = openApp(sendTo) {
                actionArray.append(mailApp)
            }
        }
        
        return actionArray
    }
    
    fileprivate func openAction(withURL: String, titleActionTitle: String) -> UIAlertAction? {
        guard let url = URL(string: withURL), UIApplication.shared.canOpenURL(url) else {
            return nil
        }
        
        let action = UIAlertAction(title: titleActionTitle, style: .default) {
            (action) in
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
        
        return action
    }
    
    fileprivate func openGmailAction(withFrom: String?) -> UIAlertAction? {
        
        var gmailUrlString = "googlegmail:///"
        
        if let from = withFrom {
            gmailUrlString += "co?to=\(from)"
        }
        
        return openAction(withURL: gmailUrlString, titleActionTitle: "Gmail")
    }
    
    fileprivate func openOutlookAction(withFrom: String?) -> UIAlertAction? {
        var mailUrlString = "ms-outlook://"
        
        if let from = withFrom {
            mailUrlString += "compose?to=\(from)"
        }
        
        return openAction(withURL: mailUrlString, titleActionTitle: "Outlook")
    }
    
    fileprivate func openMailAction(withFrom: String?) -> UIAlertAction? {
        var mailUrlString = "mailto:"
        if let from = withFrom {
            mailUrlString += "\(from)"
        }
        return openAction(withURL: mailUrlString, titleActionTitle: "Mail")
    }
    
//    fileprivate func openYahooMailAction(withFrom: String?) -> UIAlertAction? {
//        var mailUrlString = "ymail://"
//        if let from = withFrom {
//            mailUrlString += "\(from)"
//        }
//        return openAction(withURL: mailUrlString, titleActionTitle: "Yahoo Mail")
//    }
    
    fileprivate func openSparkAction(withFrom: String?) -> UIAlertAction? {
        var mailUrlString = "readdle-spark://"
        
        if let from = withFrom {
            mailUrlString += "compose?recipient=\(from)"
        }
        
        return openAction(withURL: mailUrlString, titleActionTitle: "Spark")
    }
    
    fileprivate func makeEmailAction(pair: (String, (String, String))) -> (String?) -> UIAlertAction? {
        func openEmailApp(sendTo: String?) -> UIAlertAction? {
            
            var mailUrlString = pair.1.0
            if let to = sendTo {
                mailUrlString += pair.1.1 + to
            }
            
            guard let url = URL(string: mailUrlString), UIApplication.shared.canOpenURL(url) else {
                return nil
            }
            let action = UIAlertAction(title: pair.0, style: .default) { (action) in
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            
            return action
        }
        
        return openEmailApp
    }
    
}
