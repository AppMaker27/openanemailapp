//
//  ViewController.swift
//  Open Mail
//
//  Created by Berend, Dexter on 8/20/19.
//  Copyright © 2019 Berend, Dexter. All rights reserved.
//

import UIKit

class ViewController: UIViewController, EmailChoicePresenter {
    
    var emailChoiceSheet: UIAlertController?
    
    let emailApps = [
        "Mail" : ("mailto:", ""),
        "Spark" : ("readdle-spark://", "compose?recipient="),
        "Outlook" : ("ms-outlook://", "compose?to="),
        "Gmail" : ("googlegmail:///", "co?to=")
    ]

    @IBAction func openWeb(_ sender: UIButton) {
        let recipientEmail = "test@email.com"
        if let recipientURL = URL(string: "mailto:\(recipientEmail)") {
            UIApplication.shared.open(recipientURL, options: [:],
                                      completionHandler: { (success) in
                                if success {
                                    print("Successful")
                                        }
                                } )
        }
//        let web = "http://www.formasterminds.com"
//        if let webURL = URL(string: web) {
//            let app = UIApplication.shared
//            app.open(webURL, options: [:], completionHandler: { (success) in
//                if success {
//                    print("Successful")
//                }
//            })
//        }
    }
    
    @IBAction func emailTapped(_ sender: UIButton) {
        show(emailChoiceSheet!, sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailChoiceSheet = setupEmailChoiceSheet(withFrom: "support@jcp.com")
    }


}

